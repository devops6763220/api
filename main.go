package main

import (
	"api/pb"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/spf13/viper"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"net/http"
)

type Config struct {
	Address string `mapstructure:"address"`

	GrpcServer struct {
		UserService string `mapstructure:"user_service"`
	} `mapstructure:"grpc_server"`
}

func LoadConfig() error {
	viper.SetConfigFile("configs/config.yaml")
	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		return err
	}

	err = viper.Unmarshal(&common)

	return nil
}

var (
	common      *Config
	userService pb.UserServiceClient
)

func main() {
	err := LoadConfig()
	if err != nil {
		panic(err)
	}
	userService = pb.NewUserServiceClient(connectGrpc(common.GrpcServer.UserService))
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		Response(c, "pong")
	})
	r.POST("/user", InsertUser)
	_ = r.Run(common.Address)
}

func Response(c *gin.Context, res interface{}) {
	c.JSON(http.StatusOK, gin.H{
		"message": res,
	})
}

type User struct {
	Id   uint64
	Name string
	Age  uint32
}

func InsertUser(c *gin.Context) {
	req := &pb.CreateUserRequest{
		Name: uuid.New().String(),
		Age:  uuid.New().ID(),
	}
	res, err := userService.Create(c.Request.Context(), req)
	if err != nil {
		fmt.Println("save error")
		Response(c, "error")
		return
	}
	Response(c, &User{
		Id:   res.Id,
		Name: res.Name,
		Age:  res.Age,
	})
}

func connectGrpc(domain string) *grpc.ClientConn {
	conn, err := grpc.Dial(domain,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithUnaryInterceptor(
			grpc_middleware.ChainUnaryClient(
				otelgrpc.UnaryClientInterceptor(),
			),
		),
	)
	if err != nil {
		log.Fatal("connect grpc error, domain:[%s], err:[%s]", domain, err.Error())
	}
	return conn
}
