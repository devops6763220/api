namespace := devops

helm-install:
	#kubectl create namespace ${namespace}
	kubectl create configmap api-config --from-file=configs/config.yaml --namespace ${namespace}
	helm install api deployment --namespace ${namespace}
helm-upgrade:
	helm upgrade api deployment --namespace ${namespace}


tidy:
	go mod tidy


path_proto:=./pb
proto:
	protoc --go_out=${path_proto} --go-grpc_out=${path_proto} ${path_proto}/*.proto
